var app = require('express')();
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
});
http.listen(9940, function () {
});
app.use(express.static('public'));
app.get('/killtheserverbecauseiamtom', function (req, res) {
    res.send("Server Killed.");
    process.exit(1);
});
process.setMaxListeners(0);
//VARIABLES
var playerCount = 0; // CONNECTED PLAYERS
var totalUsers = 0; //TOTAL PLAYERS SINCE START OF SERVER
// SOCKETS

io.sockets.on('connection', function (socket) {
    console.log('connect')
    ++playerCount;
    //    firstUser = socket
    var newId = idMaker();
    io.sockets.connected[socket.id].emit('constants', {homeId: newId});
    world.charCreate(newId, 'pl');

    io.sockets.emit('new_player', newId);
    io.sockets.connected[socket.id].charIds = {};
    io.sockets.connected[socket.id].homeId = newId;
    socket.on('input', function (input) {
        world.chars[io.sockets.connected[socket.id].homeId].inputAssess(input.offsetEvent,
                input.mousedown, io.sockets.connected[socket.id].homeId, socket.id);
        world.chars[io.sockets.connected[socket.id].homeId].angleBit = input.angleBit;
        world.chars[io.sockets.connected[socket.id].homeId].inputVec = input.inputVec;
        world.chars[io.sockets.connected[socket.id].homeId].angle = input.angle;
    });

    socket.on('error', function () {

    });
    socket.on('ping', function (msg) {
//        setTimeout(function () {
//
//        }, 100)
    });
    socket.on('action', function (action) {
        world.chars[io.sockets.connected[socket.id].homeId][action](socket.id);
        socket.broadcast.emit('action', {id: io.sockets.connected[socket.id].homeId,
            action: action});
    });
    socket.on('action-examine', function (action) {
        world.chars[io.sockets.connected[socket.id].homeId]['examine'](action.selected, socket.id);
    });
    socket.on('input', function (input) {
//        gameInstances[socket.galaxyId]._active[socket.shipId]._input = input;
    });
    socket.on('disconnect', function () {
        --playerCount;
    });
//    testUsers();
});

function dataSend() {
    setInterval(function () {
        var serverChars = [];
        for (var i = 0; i < world.charKeys.length; i++) {
            var currentChar = world.chars[world.charKeys[i]];
            serverChars.push(currentChar)
//            io.sockets.emit('chars', world.chars[world.charKeys[i]]);
        }
        io.sockets.emit('chars', serverChars);
    }, 40)
}
dataSend();

function findGame(galaxyId, sp) { // find empty game, triggers create if no spaces found
}

var idMaker = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
};

function World(segments, chars, rooms, c, animationLoop, entities, homeId) {
    this.segments = segments;
    this.chars = chars;
    this.rooms = rooms;
    this.c = c;
    this.animationLoop = animationLoop;
    this.entities = entities;
    this.homeId = homeId;
    this.steps = 1;
    this.segments = [];
    this.tilemap = [];
    this.xOff = 0;
    this.yOff = 0;
    this.pathNodes = [];
    this.doors = [];
    this.homes = [];
}

World.prototype.set = function () {
    return this.segments;
    return this.chars;
    return this.rooms;
    return this.c;
    return this.animationLoop;
    return this.entities;
    return this.homeId;
    return this.steps;
    return this.segments;
    return this.tilemap;
    return this.xOff;
    return this.yOff;
    return this.pathNodes;
    return this.doors;
    return this.homes;
};
function NewWorld(segments, chars, rooms, c, animationLoop, entities, homeId) {
    World.call(this, segments, chars, rooms, c, animationLoop, entities, homeId);
}

NewWorld.prototype = Object.create(World.prototype);
NewWorld.prototype.constructor = NewWorld;
NewWorld.prototype.set = function () {
    return World.prototype.set.call(this);
};

World.prototype = {
    scale: 0,
    buildingCount: 0,
    evi: {
        fingerprint: function () {
            return {expires: 600000, hName: 'fingerprint'};
        }
    },
    ents: {
        chest: function (x, y) {
            return {mName: 'chest',
                hName: 'Chest', a: 0, b: 0,
                seg: [{a: {x: x, y: y}},
                    {a: {x: x + 10, y: y}},
                    {a: {x: x + 10, y: y + 20}},
                    {a: {x: x, y: y + 20}
                    }]}
        }
    },
    template: {
        city: function (x, y, w, h) {
            return {
                seg: [{a: {x: x, y: y}},
                    {a: {x: x + w, y: y}},
                    {a: {x: x + w, y: y + h}},
                    {a: {x: x, y: y + h}
                    }]}
        },
        house: function (x, y) {
            var newHouse = {
                seg: [{a: {x: x, y: y}},
                    {a: {x: x + 160, y: y}},
                    {a: {x: x + 160, y: y + 120}},
                    {a: {x: x, y: y + 120}
                    }]}
            return newHouse
        },
        generic: function (x, y, w, h) {
            var newGeneric = {
                seg: [{a: {x: x, y: y}},
                    {a: {x: x + w, y: y}},
                    {a: {x: x + w, y: y + h}},
                    {a: {x: x, y: y + h}
                    }]}
            return newGeneric
        },
        octagon: function (x, y, r) {
            return {
                seg: [
                    {a: {x: x + 8 * r, y: y + 0 * r}},
                    {a: {x: x + 16 * r, y: y + 0 * r}},
                    {a: {x: x + 22 * r, y: y + 6 * r}},
                    {a: {x: x + 22 * r, y: y + 14 * r}},
                    {a: {x: x + 16 * r, y: y + 20 * r}},
                    {a: {x: x + 8 * r, y: y + 20 * r}},
                    {a: {x: x + 2 * r, y: y + 14 * r}},
                    {a: {x: x + 2 * r, y: y + 6 * r}}
                ]}
        }
    },
    animLoop: function () {
    },
    drawTiles: function (x, y, w, h, src) {
    },
    draw: function () {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = "rgba(0,0,0,0.8)";
        ctx.beginPath();
        ctx.rect(0, 0, 1200, 900);
        ctx.fill();
        ctx.strokeStyle = "blue";
        // Sight Polygons
        var fuzzyRadius = 10;
        var polygons = [this.getSightPolygon(world.chars[world.homeId].x,
                    world.chars[world.homeId].y)];
        for (var angle = 0; angle < Math.PI * 2; angle += (Math.PI * 2) / 10) {
            var dx = Math.cos(angle) * fuzzyRadius;
            var dy = Math.sin(angle) * fuzzyRadius;
            polygons.push(this.getSightPolygon(world.chars[world.homeId].x + dx,
                    world.chars[world.homeId].y + dy));
        }
        // DRAW AS A GIANT POLYGON
        for (var i = 1; i < polygons.length; i++) {
            this.drawPolygon(polygons[i], ctx, "rgba(255,255,255,0.2)");
        }
        this.clearPolygon(polygons[0], ctx);
        // Draw red dots


//        IS ON CLIENT SIDE
//        this.circle(Mouse.x, Mouse.y, 12)

    },
    getSightPolygon: function (sightX, sightY) {
        // Get all unique points
        var segments = world.chars[world.homeId].segments;
        var points = (function (segments) {
            var a = [];
            segments.forEach(function (seg) {
                a.push(seg.a, seg.b);
            });
            return a;
        })(segments);

        var uniquePoints = (function (points) {
            var set = {};
            return points.filter(function (p) {
                var key = p.x + "," + p.y;
                if (key in set) {
                    return false;
                } else {
                    set[key] = true;
                    return true;
                }
            });
        })(points);
        // Get all angles
        var uniqueAngles = [];
        for (var j = 0; j < uniquePoints.length; j++) {
            var uniquePoint = uniquePoints[j];
            var angle = Math.atan2(uniquePoint.y - sightY, uniquePoint.x - sightX);
            uniquePoint.angle = angle;
            uniqueAngles.push(angle - 0.00001, angle, angle + 0.00001);
        }
        // RAYS IN ALL DIRECTIONS
        var intersects = [];
        for (var j = 0; j < uniqueAngles.length; j++) {
            var angle = uniqueAngles[j];
            // Calculate dx & dy from angle
            var dx = Math.cos(angle);
            var dy = Math.sin(angle);
            // Ray from center of screen to mouse
            var ray = {
                a: {x: sightX, y: sightY},
                b: {x: sightX + dx, y: sightY + dy}
            };
            // Find CLOSEST intersection
            var closestIntersect = null;
            for (var i = 0; i < segments.length; i++) {
                var intersect = this.getIntersection(ray, segments[i]);
                if (!intersect)
                    continue;
                if (!closestIntersect || intersect.param < closestIntersect.param) {
                    closestIntersect = intersect;
                }
            }
            // Intersect angle
            if (!closestIntersect)
                continue;
            closestIntersect.angle = angle;
            // Add to list of intersects
            intersects.push(closestIntersect);
        }
        // Sort intersects by angle
        intersects = intersects.sort(function (a, b) {
            return a.angle - b.angle;
        });
        // Polygon is intersects, in order of angle
        return intersects;
    },
    getIntersection: function (ray, segment) {
        // RAY in parametric: Point + Delta*T1
        var r_px = ray.a.x;
        var r_py = ray.a.y;
        var r_dx = ray.b.x - ray.a.x;
        var r_dy = ray.b.y - ray.a.y;
        // SEGMENT in parametric: Point + Delta*T2
        var s_px = segment.a.x;
        var s_py = segment.a.y;
        var s_dx = segment.b.x - segment.a.x;
        var s_dy = segment.b.y - segment.a.y;
        // Are they parallel? If so, no intersect
        var r_mag = Math.sqrt(r_dx * r_dx + r_dy * r_dy);
        var s_mag = Math.sqrt(s_dx * s_dx + s_dy * s_dy);
        if (r_dx / r_mag == s_dx / s_mag && r_dy / r_mag == s_dy / s_mag) {
            // Unit vectors are the same.
            return null;
        }
        // SOLVE FOR T1 & T2
        // r_px+r_dx*T1 = s_px+s_dx*T2 && r_py+r_dy*T1 = s_py+s_dy*T2
        // ==> T1 = (s_px+s_dx*T2-r_px)/r_dx = (s_py+s_dy*T2-r_py)/r_dy
        // ==> s_px*r_dy + s_dx*T2*r_dy - r_px*r_dy = s_py*r_dx + s_dy*T2*r_dx - r_py*r_dx
        // ==> T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
        var T2 = (r_dx * (s_py - r_py) + r_dy * (r_px - s_px)) / (s_dx * r_dy - s_dy * r_dx);
        var T1 = (s_px + s_dx * T2 - r_px) / r_dx;
        // Must be within parametic whatevers for RAY/SEGMENT
        if (T1 < 0)
            return null;
        if (T2 < 0 || T2 > 1)
            return null;
        // Return the POINT OF INTERSECTION

        return {
            x: r_px + r_dx * T1,
            y: r_py + r_dy * T1,
            param: T1
        };
    },
    drawPolygon: function (polygon, ctx, fillStyle) {
        ctx.fillStyle = fillStyle;
        ctx.beginPath();
        ctx.moveTo(polygon[0].x, polygon[0].y);
        for (var i = 1; i < polygon.length; i++) {
            var intersect = polygon[i];
            ctx.lineTo(intersect.x, intersect.y);
        }
        ctx.fill();
    },
    clearPolygon: function (polygon, ctx, fillStyle) {
        ctx.globalCompositeOperation = 'destination-out';
        ctx.beginPath();
        ctx.moveTo(polygon[0].x, polygon[0].y);
        for (var i = 1; i < polygon.length; i++) {
            var intersect = polygon[i];
            ctx.lineTo(intersect.x, intersect.y);
        }
        ctx.closePath();
        ctx.fill();
        ctx.globalCompositeOperation = 'source-over';
    },
    clear: function () {
        this.c.ctx.clearRect(0, 0, this.c.w, this.c.h);
    },
    circle: function (x, y, r) {
        this.c.ctx.beginPath();
        this.c.ctx.fillStyle = "red";
        this.c.ctx.arc(x, y, r, 0, 2 * Math.PI);
        this.c.ctx.fill();
    },
    line: function (o, p) {
        this.c.ctx.beginPath();
        this.c.ctx.moveTo(o.x, o.y);
        this.c.ctx.lineTo(p.x, p.y);
        this.c.ctx.stroke();
    },
    img: function (o) {
        this.c.ctx.drawImage(o.img, o.x - o.w / 2, o.y - o.h / 2, o.w, o.h);
    },
    rect: function (x, y, w, h, fill) {
        this.c.ctx.beginPath();
        this.c.ctx.rect(x, y, w, h);
        if (fill !== undefined && fill !== 'none') {
            this.c.ctx.fillStyle = fill;
            this.c.ctx.fill();
        } else {
            this.c.ctx.stroke();
        }
    },
    drawRooms: function (x, y) {
        for (var i = 0; i < world.rooms.length; i++) {
            ctx.beginPath();
            ctx.lineWidth = 5;
            ctx.strokeStyle = 'black';
            ctx.moveTo(world.rooms[i].seg[0].a.x,
                    world.rooms[i].seg[0].a.y);
            for (var j = 0; j < world.rooms[i].seg.length; j++) {
                ctx.lineTo(world.rooms[i].seg[j].b.x,
                        world.rooms[i].seg[j].b.y);
                ctx.fillStyle = 'grey';
                if (this.chars[this.homeId].inside !== world.rooms[i] &&
                        i !== 0) {
                    ctx.fill();
                }
            }
            ctx.closePath();
            ctx.stroke();
            if (world.rooms[i].door !== undefined) {
                if (world.rooms[i] !== world.chars[world.homeId].inside &&
                        world.rooms[i].parent === 0 &&
                        world.chars[world.homeId].inside.parent !== world.rooms[i]) {
                    ctx.font = "20px Arial";
                    ctx.fillStyle = 'black';
                    ctx.fillText(world.rooms[i].hName, world.rooms[i].seg[0].a.x + 10,
                            world.rooms[i].seg[0].a.y + 30);
                }
                ctx.beginPath();
                ctx.lineWidth = 1;
                ctx.arc(world.rooms[i].door.x, world.rooms[i].door.y, 20, 0, 1.5);
                ctx.stroke();
                ctx.beginPath();
                ctx.lineWidth = 4;
                ctx.moveTo(world.rooms[i].door.x, world.rooms[i].door.y + 20);
                ctx.lineTo(world.rooms[i].door.x, world.rooms[i].door.y);
                ctx.lineTo(world.rooms[i].door.x + 20, world.rooms[i].door.y);
                ctx.stroke();
            }
        }
        var closeEnts = world.chars[world.homeId].inside.entities;
        for (var i = 0; i < closeEnts.length; i++) {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.strokeStyle = 'black';
            ctx.moveTo(closeEnts[i].seg[0].a.x,
                    closeEnts[i].seg[0].a.y);
            for (var j = 0; j < closeEnts[i].seg.length; j++) {
                ctx.lineTo(closeEnts[i].seg[j].b.x,
                        closeEnts[i].seg[j].b.y);
            }
            ctx.fillStyle = 'white';
            ctx.stroke();
            ctx.fill();
        }
    },
    idMaker: function () {
        return '_' + Math.random().toString(36).substr(2, 9);
    },
    charCreate: function (id, tag) {
//        for(var i = 0; i < world.homes.length; i++){
//            if(world.homes[i].resident.length === 0){
//                vacantHome = i;
//            }
//        }
//        world.homes[vacantHome].resident = id;
        var home =  this.rooms[4];
//        var home =  this.rooms[world.homes[vacantHome].index];
        this.charKeys.push(id);
        this.chars[id] = new this.Char(id, 'test', 0, 0,
                home.x+80, home.y+60, -1, 20, 20, 0, home);
        this.chars[id].segmentBuild();
        this.chars[id].storage(new this.Item({parent: id, b: 0, range: 50,
            damage: 1, effect: -1}));
        var vacantHome = '';
    },
    charKeys: [],
    charGet: function (id) {
        for (var i = 0; i < this.charKeys.length; i++) {
            if (id === this.charKeys[i]) {
                return chars[this.charKeys[i]];
            }
        }
    },
    charFn: function () {
        for (var i = 0; i < this.charKeys.length; i++) {
            var ch = this.chars[this.charKeys[i]];
            ch.pathStep();
            for (var j = 0; j < this.charKeys.length; j++) {
                var ch2 = this.chars[this.charKeys[j]];
                if (i < j) {
                    ch.charCol(ch, ch2);
                }
//            this.img(ch);
            }
            ch.tileCol();
            ch.move();
        }
    },
    inVision: function (ob) {
//        var polygons = world.getSightPolygon(world.chars[world.homeId].x,
//                    world.chars[world.homeId].y);
//        for (var i = 0; i < polygons.length; i++) {
//            var dist = distance(polygons[0], polygons[i]);
//            var angle = findAngle(polygons[0], polygons[i], dist);
//            for (var j = 0; j < 100; j++) {
//                var point = {x: polygons[i].x + (dist / 100 * j * Math.cos(Math.PI / 180 * angle)),
//                    y: polygons[i].y + (dist / 100 * j * Math.sin(Math.PI / 180 * angle))};
//                if (distance(ob, point) < 13) {
//                }
//            }
//        }
    },
    charInterp: function (serverChars) {
        for (var i = 0; i < serverChars.length; i++) {
            if (!this.chars[serverChars[i].id]) {
                world.charCreate(serverChars[i].id);
            }
//            if (this.chars[serverChars[i].id] && serverChars[i].id !== this.homeId) {
            this.chars[serverChars[i].id].x =
                    (this.chars[serverChars[i].id].x * 9 + serverChars[i].x) / 10;
            this.chars[serverChars[i].id].y =
                    (this.chars[serverChars[i].id].y * 9 + serverChars[i].y) / 10;
            this.chars[serverChars[i].id].vX =
                    (this.chars[serverChars[i].id].vX * 9 + serverChars[i].vX) / 10;
            this.chars[serverChars[i].id].vY =
                    (this.chars[serverChars[i].id].vY * 9 + serverChars[i].vY) / 10;
            this.chars[serverChars[i].id].angleBit = serverChars[i].angleBit;
            this.chars[serverChars[i].id].inputVec = serverChars[i].inputVec;
            if (serverChars[i].angleBit !== 0) {
            }
//            }
        }
    },
    createRooms: function () {
        // needs level editor
        //make x shaped rooms have them assemble in the create functions 
        //add objects to rooms objects need x y angle store
        this.roomKeys = [];
        this.rooms.push(new this.Room(world.template.city(20, 20, 1200, 800), -1, 'City',
                'city'));
        this.rooms.push(new this.Room(world.template.house(60, 60), 0, 'House 1',
                'house', 2));
        this.doors.push(new this.Door(this.rooms.length - 1, 2, 30));
        this.entities.push(new this.Entity(world.ents.chest(4, 4), this.rooms.length - 1, world.storage));
//        this.rooms[1].entities[0].storage(new this.Item(this.rooms[1].entities[0]));
        this.rooms.push(new this.Room(world.template.house(280, 60), 0, 'House 2',
                'house', 2));
        this.doors.push(new this.Door(this.rooms.length - 1, 2, 30));
        this.rooms.push(new this.Room(world.template.house(60, 500), 0, 'House 3',
                'house', 2));
        this.doors.push(new this.Door(this.rooms.length - 1, 1, 30));
        this.rooms.push(new this.Room(world.template.house(280, 500), 0, 'House 4',
                'house', 2));
        this.doors.push(new this.Door(this.rooms.length - 1, 1, 30));
        this.rooms.push(new this.Room({
            seg: [{a: {x: 600, y: 600}},
                {a: {x: 900, y: 600}},
                {a: {x: 900, y: 700}},
                {a: {x: 750, y: 650}},
                {a: {x: 600, y: 700}}
            ]}, 0, 'House 4',
                'house', 3));
        this.rooms.push(new this.Room(world.template.octagon(500, 50, 5), 0, 'The Pantry',
                'the_pantry', 5));
        this.doors.push(new this.Door(this.rooms.length - 1, 1, 30));
        this.rooms.push(new this.Room(world.template.octagon(800, 50, 5), 0, 'The Pantry',
                'the_pantry', 5));
        this.doors.push(new this.Door(this.rooms.length - 1, 1, 30));
        this.rooms.push(new this.Room(world.template.octagon(190, 280, 5), 0));
        this.rooms.push(new this.Room(world.template.octagon(110, 340, 2), 0));
        this.rooms.push(new this.Room(world.template.octagon(345, 340, 2), 0));
        this.rooms.push(new this.Room(world.template.octagon(190, 720, 5), 0));
        this.rooms.push(new this.Room(world.template.octagon(110, 750, 2), 0));
        this.rooms.push(new this.Room(world.template.octagon(345, 750, 2), 0));
        this.rooms.push(new this.Room(world.template.generic(480, 180, 400, 240), 0,
                'The Offices', 'generic', 0));
        this.doors.push(new this.Door(this.rooms.length - 1, 0, 30));
        this.doors.push(new this.Door(this.rooms.length - 1, 2, 30));
        this.rooms.push(new this.Room(world.template.generic(0, 0, 120, 120), this.rooms.length - 1,
                '', 'office', 2));
        this.doors.push(new this.Door(this.rooms.length - 1, 2, 30));
    },
    homesSetup: function () {
        for (var i = 0; i < world.rooms.length; i++) {
            if (world.rooms[i].mName === 'house')
                world.homes.push({index: i, resident: ''});
        }
    },
    worldSegmentBuild: function () {
        var segments = [];
        world.inside = world.rooms[0];
        var visibleRooms = world.rooms;
        for (var i = 0; i < visibleRooms.length; i++) {
            for (var j = 0; j < visibleRooms[i].seg.length; j++) {
                segments.push(visibleRooms[i].seg[j]);
            }
//            for (var k = 0; k < this.inside.entities.length; k++) {
//                for (var h = 0; h < this.inside.entities[k].seg.length; h++) {
//                    segments.push(this.inside.entities[k].seg[h]);
//                }
//            }
        }
        for (var j = 0; j < this.inside.seg.length; j++) {
            segments.push(this.inside.seg[j]);
        }
        world.segments = segments;
    },
    segmentBuild: function () {
        var segments = [];
        var visibleRooms = this.inside.children;
        for (var i = 0; i < visibleRooms.length; i++) {
            for (var j = 0; j < visibleRooms[i].seg.length; j++) {
                segments.push(visibleRooms[i].seg[j]);
            }
        }
        for (var k = 0; k < this.inside.entities.length; k++) {
            for (var h = 0; h < this.inside.entities[k].seg.length; h++) {
                segments.push(this.inside.entities[k].seg[h]);
            }
        }
        for (var j = 0; j < this.inside.seg.length; j++) {
            segments.push(this.inside.seg[j]);
        }
        this.segments = segments;
    },
    middler: function (halfSegs) {
        var midX = 0;
        var midY = 0;
        for (var j = 0; j < halfSegs.length; j++) {
            midX += halfSegs[j].a.x;
            midY += halfSegs[j].a.y;
        }
        return {x: midX / halfSegs.length, y: midY / halfSegs.length}
    },
    setPolyVectors: function (halfSegs, parent) {
        var segs = [];
        if (parent >= 0) {
            for (var i = 0; i < halfSegs.length; i++) {
                halfSegs[i].a.x += world.rooms[parent].x;
                halfSegs[i].a.y += world.rooms[parent].y;
            }
        }
        for (var j = 0; j < halfSegs.length; j++) {
            if (j === halfSegs.length - 1) {
                halfSegs[j].b = halfSegs[0].a;
            } else {
                halfSegs[j].b = halfSegs[j + 1].a;
            }
            var dist = distance(halfSegs[j].a, halfSegs[j].b);
            var angle = findAngle(halfSegs[j].a, halfSegs[j].b, dist);
            var center = {x: halfSegs[j].a.x + (dist * 0.5 * Math.cos(Math.PI / 180 * angle)),
                y: halfSegs[j].a.y + (dist * 0.5 * Math.sin(Math.PI / 180 * angle))};
            segs.push({a: {x: halfSegs[j].a.x, y: halfSegs[j].a.y}, center: center,
                b: {x: halfSegs[j].b.x, y: halfSegs[j].b.y}, dist: dist, angle: angle});
        }
        return  segs;
    },
    lineIntersect: function (x1, y1, x2, y2, x3, y3, x4, y4) {
        var x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
        var y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
        if (isNaN(x) || isNaN(y)) {
            return false;
        } else {
            if (x1 >= x2) {
                if (!(x2 <= x && x <= x1)) {
                    return false;
                }
            } else {
                if (!(x1 <= x && x <= x2)) {
                    return false;
                }
            }
            if (y1 >= y2) {
                if (!(y2 <= y && y <= y1)) {
                    return false;
                }
            } else {
                if (!(y1 <= y && y <= y2)) {
                    return false;
                }
            }
            if (x3 >= x4) {
                if (!(x4 <= x && x <= x3)) {
                    return false;
                }
            } else {
                if (!(x3 <= x && x <= x4)) {
                    return false;
                }
            }
            if (y3 >= y4) {
                if (!(y4 <= y && y <= y3)) {
                    return false;
                }
            } else {
                if (!(y3 <= y && y <= y4)) {
                    return false;
                }
            }
        }
        return true;
    },
    setPathNodes: function () {
        var segments = world.segments;
        for (var i = 0; i < segments.length; i++) {
            world.pathNodes.push(
                    {x: segments[i].a.x + (30 * Math.cos(Math.PI / 180 * (segments[i].angle - 180))),
                        y: segments[i].a.y + (30 * Math.sin(Math.PI / 180 * (segments[i].angle - 180))),
                        neighbours: [], path: 1, index: 0});
            world.pathNodes.push(
                    {x: segments[i].b.x + (30 * Math.cos(Math.PI / 180 * segments[i].angle)),
                        y: segments[i].b.y + (30 * Math.sin(Math.PI / 180 * segments[i].angle)),
                        neighbours: [], path: 1, index: 0});
            world.pathNodes.push(
                    {x: segments[i].center.x + (30 * Math.cos(Math.PI / 180 * (segments[i].angle + 90))),
                        y: segments[i].center.y + (30 * Math.sin(Math.PI / 180 * (segments[i].angle + 90))),
                        neighbours: [], path: 1, index: 0});
            world.pathNodes.push(
                    {x: segments[i].center.x + (30 * Math.cos(Math.PI / 180 * (segments[i].angle - 90))),
                        y: segments[i].center.y + (30 * Math.sin(Math.PI / 180 * (segments[i].angle - 90))),
                        neighbours: [], path: 1, index: 0});
        }
//            for (var i = 0; i < 13; i++) {
//                for (var j = 0; j < 8; j++) {
//                    world.pathNodes.push(
//                            {x: i * (100 + Math.random()), y: j * (100 + Math.random()),
//                                neighbours: [], path: 1, index: count});
//                    ++count;
//                }
//            }
        for (var i = 0; i < world.pathNodes.length; i++) {
            for (var j = 0; j < world.pathNodes.length; j++) {
                if (distance(world.pathNodes[i], world.pathNodes[j]) < 20
                        && i !== j) {
                    world.pathNodes.splice(j, 1);
                }
            }
        }
        for (var i = 0; i < world.doors.length; i++) {
            var usableDoor = world.doors[i];
            if (usableDoor !== undefined) {
                world.pathNodes.push(
                        {x: usableDoor.x + (20 * Math.cos(Math.PI / 180 * usableDoor.angle)),
                            y: usableDoor.y + (20 * Math.sin(Math.PI / 180 * usableDoor.angle)),
                            neighbours: [], path: 2, index: 0});
                world.pathNodes.push(
                        {x: usableDoor.x + (20 * Math.cos(Math.PI / 180 * (usableDoor.angle + 180))),
                            y: usableDoor.y + (20 * Math.sin(Math.PI / 180 * (usableDoor.angle + 180))),
                            neighbours: [], path: 2, index: 0});
            }
        }
        for (var i = 0; i < world.pathNodes.length; i++) {
            world.pathNodes[i].index = i;
            for (var j = 0; j < world.pathNodes.length; j++) {
                if (world.isVisible(world.pathNodes[i], world.pathNodes[j]) === true
                        && i !== j) {
                    world.pathNodes[i].neighbours.push(j);
                }
            }
        }
        for (var i = 0; i < world.pathNodes.length - 1; i += 2) {
            if (world.pathNodes[i].path === 2) {
                world.pathNodes[i].neighbours.push(i + 1);
                world.pathNodes[i + 1].neighbours.push(i);
            }
        }
    },
    isVisible: function (a, b) {
        for (var h = 0; h < world.segments.length; h++) {
            var angle = findAngle(a, b, distance(a, b));
            var fuzzyRadius = 13;
            for (var angle = 0; angle < Math.PI * 2; angle += (Math.PI * 2) / 10) {
                var dx = Math.cos(angle) * fuzzyRadius;
                var dy = Math.sin(angle) * fuzzyRadius;
                var intersect = world.lineIntersect(a.x + dx, a.y + dy, b.x + dx, b.y + dy,
                        world.segments[h].a.x + dx / 10, world.segments[h].a.y + dy / 10,
                        world.segments[h].b.x + dx / 10, world.segments[h].b.y + dy / 10);
                if (intersect === true) {
                    return false;
                }
                var intersect = world.lineIntersect(a.x + dx, a.y + dy, b.x - dx, b.y - dy,
                        world.segments[h].a.x + dx / 10, world.segments[h].a.y + dy / 10,
                        world.segments[h].b.x - dx / 10, world.segments[h].b.y - dy / 10);
                if (intersect === true) {
                    return false;
                }
            }
        }
        return true;
    },
    storage: function (item) {
        this.store[0] = item;
    },
    rand: function (min, max) {
        return Math.random() * (max - min) + min;
    },
    mouseOverAssess: function (offsetEvent, mouseDown, id) {
        var homeChar = world.chars[id];
        var ents = world.entities;
        var over = [];
        for (var i = 0; i < world.charKeys.length; i++) {
            var ch = world.chars[world.charKeys[i]];
            if (distance(ch, offsetEvent) < ch.size + 6 && ch.id !== homeChar.id) {
                over.push({tag: 'chars', index: ch.id});
            }
        }
        for (var i = 0; i < ents.length; i++) {
            if (distance(offsetEvent, ents[i]) < ents[i].size + 10) {
                over.push({tag: 'ents', index: i});
            }
        }
        for (var i = 0; i < world.doors.length; i++) {
            if (distance(offsetEvent, world.doors[i]) < world.doors[i].size + 10) {
                over.push({tag: 'doors', index: i});
            }
        }
        return over;
    },
};

World.prototype.Char = function (id, img, vX, vY, x, y, angle, w, h, angleBit, inside, insideIndex) {
    this.id = id;
    this.img = img;
    this.vX = vX;
    this.vY = vY;
    this.x = x;
    this.y = y;
    this.angle = 90;
    this.w = w;
    this.h = h;
    this.angleBit = angleBit;
    this.inside = inside;
    this.store = [{}, {}, {}, {}];
    this.storage = world.storage;
    this.canSee = [];
    this.times = {delta: process.hrtime(), lastFrame: process.hrtime()};
    this.segmentBuild = world.segmentBuild;
    this.path = [];
    this.destination = {x: 0, y: 0};
    this.inputVec = -1;
    this.size = 12.5;
    this.speed = 0;
    this.moveAngle = 0;
    this.evidence = [];
    this.selected = {x: 0, y: 0};
    this.samples = [];
    this.home = {};
};

World.prototype.Char.prototype = {
    input: {

    },
    inputAssess: function (offsetEvent, mouseDown, id, socketId) {
        var homeChar = world.chars[id];
        var ents = world.entities;
        var confirm = {};
        var mOver = world.mouseOverAssess(offsetEvent, mouseDown, id);
        if (mOver.length > 0) {
            var ov = world[mOver[0].tag][mOver[0].index];
            this.selected = ov;
            if (mouseDown === 0) {
                io.sockets.connected[socketId].emit('selected', mOver[0]);
            }
            if (mouseDown === 2) {
                if (mOver[0].tag === 'doors') {
                    if (ov === this.selected) {
                        this.examine(ov, socketId);
                    } else {
//                        this.useDoor();
                    }
                }
                if (mOver[0].tag === 'chars') {
                    if (ov === this.selected) {
                        this.talk(ov, socketId);
                    } else {
//                        this.useDoor();
                    }
                }
                if (mOver[0].tag === 'ents') {
                    if (ov === this.selected) {
                        this.defAction(ov, socketId);
                    } else {
//                        this.useDoor();
                    }
                }
            }
        }
    },
    turning: function () {
        var angleDiff = 0;
        var dir = this.angle - this.inputVec;
        if (dir < 15 && dir > -15) {
            this.angle = this.inputVec;
            return;
        }
        if (dir > 0 && Math.abs(dir) <= 180) {
            this.angle -= 15;
            this.speed *= 0.8;
        } else if (dir > 0 && Math.abs(dir) > 180) {
            this.angle += 15;
            this.speed *= 0.8;
        } else if (dir < 0 && Math.abs(dir) <= 180) {
            this.angle += 15;
            this.speed *= 0.8;
        } else if (dir < 0 && Math.abs(dir) > 180) {
            this.angle -= 15;
            this.speed *= 0.8;
        }
        while (this.angle > 360) {
            this.angle -= 360;
        }
        while (this.angle < 0) {
            this.angle += 360;
        }
    },
    pathStep: function () {
        if (this.path.length > 0) {
            if (this.path.length > 0 && distance(this.path[0], this) <= 10) {
                if (this.path[0].path === 2) {
                    this.useDoor();
                    this.path.shift();
                }
                this.path.shift();
                if (this.path.length === 0) {
                    this.angleBit = 0;
                    this.path = [];
                    return;
                }
            }
            this.inputVec = findAngle(this, this.path[0]);
            this.angleBit = 1;
        }
    },
    talk: function (ob, socketId) {
        if (distance(this, ob) < 50) {
        }
    },
    move: function () {
        if (isNaN(this.inputVec) === true || isNaN(this.angle) === true) {
            // COULD EQUAL NaN IF DISTANCE IS 0
            this.inputVec = 1;
            this.angle = 1;
            this.angleBit = 0;
        }
        this.turning();
//        this.angle = this.inputVec;
        if (this.angleBit === 0) {
            this.speed = Math.max(this.speed * 0.75 - 0.2, 0);
            // no input, declerating
        } else {
            // accelerating
            this.speed = Math.min(0.05 + this.speed * 1.01, 1);
        }
        this.vX = this.speed * Math.cos(Math.PI / 180 * this.angle);
        this.vY = this.speed * Math.sin(Math.PI / 180 * this.angle);
        this.x += this.vX * this.times.delta[1] * 1e-7;
        this.y += this.vY * this.times.delta[1] * 1e-7;
        this.times.delta = process.hrtime(this.times.lastFrame);
        this.times.lastFrame = process.hrtime();
//        this.vX -= (this.vX * 0.5);
//        this.vY -= (this.vY * 0.5);
    },
    actions: function () {
        this.useDoor();
        this.attack();
    },
    examine: function (ob, socketId) {
        if (distance(this, ob) < 50) {
            console.log(ob.evidence);
            io.sockets.connected[socketId].emit('examined', ob.evidence);
        }
    },
    useDoor: function () {
        for (var i = 0; i < world.doors.length; i++) {
            var usableDoor = world.doors[i];
            if (usableDoor !== undefined && distance(this, usableDoor) < 50) {
                var angle = usableDoor.angle;
                if (this.inside === world.rooms[usableDoor.parent]) {
                    this.inside = world.rooms[usableDoor.grandParent];
                } else {
                    this.inside = world.rooms[usableDoor.parent];
                    angle -= 180;
                }
                this.x = usableDoor.x + 20 * Math.cos(Math.PI / 180 * angle);
                this.y = usableDoor.y + 20 * Math.sin(Math.PI / 180 * angle);
                usableDoor.evidence.push(new world.Evidence(this.id, world.evi.fingerprint()))
                this.segmentBuild();
            }
        }
    },
    attack: function () {
        if (distance(this, world.chars[Object.keys(world.chars)[0]]) <=
                this.store[Object.keys(this.store)[0]].range) {
        }
    },
    charCol: function (ch1, ch2) {
        if (distance(ch1, ch2) < ch1.size + ch2.size) {

        }
    },
    tileCol: function () {
        for (var i = 0; i < this.segments.length; i++) {
            if (distance(this, this.segments[i].center) < this.segments[i].dist / 2) {
                for (var j = 0; j < 100; j++) {
                    var point = {x: this.segments[i].a.x + (this.segments[i].dist / 100 * j *
                                Math.cos(Math.PI / 180 * this.segments[i].angle)),
                        y: this.segments[i].a.y + (this.segments[i].dist / 100 * j *
                                Math.sin(Math.PI / 180 * this.segments[i].angle))};
                    if (distance(this, point) < 13) {
                        var sw = 0;
                        for (var k = 0; k < this.inside.seg.length; k++) {
                            if (this.inside.seg[k].a.x === this.segments[i].a.x &&
                                    this.inside.seg[k].a.y === this.segments[i].a.y) {
                                sw = 180;
                            }
                        }
                        this.angle = this.segments[i].angle - 90 + sw;
                        this.vX = 1 * Math.cos(Math.PI / 180 * this.angle);
                        this.vY = 1 * Math.sin(Math.PI / 180 * this.angle);
                        this.x += this.vX;
                        this.y += this.vY;
                    }
                }
            }
        }
    },
    calculatePath: function (pathStart, pathEnd) {
        var start = {x: pathStart.x, y: pathStart.y, neighbours: [],
            index: world.pathNodes.length};
        var end = {x: pathEnd.x, y: pathEnd.y, neighbours: [],
            index: world.pathNodes.length + 1};
        var board = new Array();
        if (world.isVisible(start, end) === true) {
            this.path = [end];
            return;
        }
        for (var i = 0; i < world.pathNodes.length; i++) {
            board.push(JSON.parse(JSON.stringify(world.pathNodes[i])));
            if (world.isVisible(world.pathNodes[i], start) === true) {
                start.neighbours.push(i);
                board[i].neighbours.push(world.pathNodes.length);
            }
            if (world.isVisible(world.pathNodes[i], end) === true) {
                end.neighbours.push(i);
                board[i].neighbours.push(world.pathNodes.length + 1);
            }
        }
        board.push(start, end);
        // create Nodes from the Start and End x,y coordinates
        var mypathStart = Node(null, start);
        var mypathEnd = Node(null, end);
        // create an array that will contain all world cells
        var AStar = new Array(1768);
        // list of currently open Nodes
        var Open = [mypathStart];
        // list of closed Nodes
        var Closed = [];
        // list of the final output array
        var result = [];
        // reference to a Node (that is nearby)
        var myNeighbours = [];
        // reference to a Node (that we are considering now)
        var myNode;
        // reference to a Node (that starts a path in question)
        var myPath;
        // temp integer variables used in the calculations
        var length, max, min, i, j;
        // iterate through the open list until none are left
        var gridNode;
        while (length = Open.length) {
            max = 1768;
            min = -1;
            for (var i = 0; i < length; i++)
            {
                if (Open[i].f < max)
                {
                    max = Open[i].f;
                    min = i;
                }
            }
            // grab the next node and remove it from Open array
            myNode = Open.splice(min, 1)[0];
            // is it the destination node?
            if (myNode.value === mypathEnd.value)
            {
                myPath = Closed[Closed.push(myNode) - 1];
                do
                {
                    result.push({x: myPath.x, y: myPath.y, path: myPath.path});
                } while (myPath = myPath.Parent);
                // clear the working arrays
                AStar = Closed = Open = [];
                // we want to return start to finish
                result.reverse();
            } else { // not the destination           
                // find which nearby nodes are walkable
                gridNode = {x: myNode.x, y: myNode.y, path: myNode.path};
                var myNeighbours = [];
                for (var k = 0; k < myNode.neighbours.length; k++) {
                    myNeighbours.push(board[board[myNode.neighbours[k]].index])
                }
                // test each one that hasn't been tried already
                for (var i = 0, j = myNeighbours.length; i < j; i++) {
                    myPath = Node(myNode, myNeighbours[i]);
                    if (!AStar[myPath.value])
                    {
                        // estimated cost of this particular route so far
                        myPath.g = myNode.g + distance(myNeighbours[i], myNode);
                        // estimated cost of entire guessed route to the destination
                        myPath.f = myPath.g + distance(myNeighbours[i], mypathEnd);
                        // remember this new path for testing above
                        Open.push(myPath);
                        AStar[myPath.value] = true;
                    }
                }
                // remember this route as having no more untested options
                Closed.push(myNode);
            }
        } // keep iterating until until the Open list is empty    
//        result.splice(0, 1);
//        lastPath = JSON.parse(JSON.stringify(result));
        this.path = result;
    },
};

World.prototype.Char.prototype.set = function () {
    return this.id;
    return this.img;
    return this.vX;
    return this.vY;
    return this.x;
    return this.y;
    return this.angle;
    return this.w;
    return this.h;
    return this.angleBit;
    return this.inside;
    return this.store;
    return this.storage;
    return this.canSee;
    return this.times;
    return this.segmentBuild;
    return this.path;
    return this.destination;
    return this.inputVec;
    return this.size;
    return this.speed;
    return this.moveAngle;
    return this.evidence;
    return this.selected;
    return this.home;
}
World.prototype.NewChar = function (id, img, vX, vY, x, y, angle, w, h, angleBit, inside, store) {
    World.prototype.Char.call(this, id, img, vX, vY, x, y, angle, w, h, angleBit, inside, store);
}

World.prototype.NewChar.prototype = Object.create(World.prototype.Char.prototype);
World.prototype.NewChar.prototype.constructor = World.prototype.NewChar;
World.prototype.NewChar.prototype.set = function () {
    return World.prototype.Char.prototype.set.call(this);
};

World.prototype.Room = function (newRoom, parent, hName, mName, door, gName) {
    var fullSegs = world.setPolyVectors(newRoom.seg, parent);
    this.hName = hName;
    this.mName = mName;
    this.gName = gName;
    this.seg = fullSegs;
    this.entities = [];
    this.door = undefined;
    if (door !== undefined) {
        this.door = {x: fullSegs[door].center.x, y: fullSegs[door].center.y,
            angle: fullSegs[door].angle - 90, id: world.buildingCount};
    }
    this.x = newRoom.seg[0].a.x;
    this.y = newRoom.seg[0].a.y;
    this.parent = {};
    this.children = [];
    if (parent >= 0) {
        world.rooms[parent].children.push(this);
        this.parent = parent;
    }
    this.evidence = [];;
    this.index = world.buildingCount;
    ++world.buildingCount;
};

World.prototype.Room.set = function () {
    return this.hName;
    return this.mName;
    return this.gName;
    return this.seg;
    return this.entities;
    return this.door;
    return this.x;
    return this.y;
    return this.parent;
    return this.children;
    return this.evidence;
    return this.index;
};

World.prototype.NewRoom = function (newRoom, parent, hName, mName, door, gName) {
    Room.call(this, newRoom, parent, hName, mName, door, gName);
}

World.prototype.NewRoom.prototype = Object.create(World.prototype.Room.prototype);
World.prototype.NewRoom.prototype.constructor = World.prototype.NewRoom;
World.prototype.NewRoom.prototype.set = function () {
    return World.prototype.Room.prototype.set.call(this);
};
World.prototype.Room.prototype = {

}

World.prototype.Entity = function (newEntity, parent, defAction) {
    this.a = newEntity.a;
    this.b = newEntity.b;
    this.parent = parent;
    this.seg = world.setPolyVectors(newEntity.seg, parent);
    this.store = [{}];
    this.middle = world.middler(newEntity.seg);
    this.storage = world.storage;
    this.size = 10;
    this.x = world.middler(newEntity.seg).x;
    this.y = world.middler(newEntity.seg).y;
    this.evidence = [];
    this.defAction = defAction
};
World.prototype.Entity.set = function () {
    return this.a;
    return this.b;
    return this.parent;
    return this.seg;
    return this.store;
    return this.middle;
    return this.storage;
    return this.size;
    return this.x;
    return this.y;
    return this.evidence;
    return this.defAction;
};

World.prototype.NewEntity = function (newEntity, parent, defAction) {
    Entity.call(this, newEntity, parent, defAction);
}


World.prototype.NewEntity.prototype = Object.create(World.prototype.Entity.prototype);
World.prototype.NewEntity.prototype.constructor = World.prototype.NewEntity;
World.prototype.NewEntity.prototype.set = function () {
    return World.prototype.Entity.prototype.set.call(this);
};


World.prototype.Item = function (newItem) {
    this.parent = newItem.parent;
    this.b = newItem.b;
    this.range = newItem.range;
    this.damage = newItem.damage;
    this.effect = newItem.effect;
    this.evidence = [];
};

World.prototype.Item.set = function () {
    return this.parent;
    return this.b;
    return this.range;
    return this.damage;
    return this.effect;
    return this.evidence;
};

World.prototype.NewItem = function (newItem) {
    Item.call(this, newItem);
}

World.prototype.NewItem.prototype = Object.create(World.prototype.Item.prototype);
World.prototype.NewItem.prototype.constructor = World.prototype.NewItem;
World.prototype.NewItem.prototype.set = function () {
    return World.prototype.Item.prototype.set.call(this);
};

World.prototype.Door = function (parent, segment, position) {
    this.parent = parent;
    this.segment = segment;
    this.position = position;
    this.x = world.rooms[parent].seg[segment].center.x;
    this.y = world.rooms[parent].seg[segment].center.y;
    this.angle = world.rooms[parent].seg[segment].angle - 90;
    this.grandParent = world.rooms[parent].parent;
    this.evidence = [];
    this.size = 10;
};
World.prototype.Door.set = function () {
    return this.parent;
    return this.segment;
    return this.position;
    return this.x;
    return this.y;
    return this.angle;
    return this.grandParent;
    return this.evidence;
    return this.size;
};

World.prototype.NewDoor = function (parent, segment, position) {
    Door.call(this, parent, segment, position);
};

World.prototype.NewDoor.prototype = Object.create(World.prototype.Door.prototype);
World.prototype.NewDoor.prototype.constructor = World.prototype.NewDoor;
World.prototype.NewDoor.prototype.set = function () {
    return World.prototype.Door.prototype.set.call(this);
};

World.prototype.Evidence = function (parent, template) {
    var newId = idMaker();
    this.parent = parent;
    this.id = newId;
    this.expires = template.expires;
    this.hName = template.hName;
};

World.prototype.Evidence.set = function () {
    return this.parent;
    return this.id;
    return this.expires;
    return this.hName;
};

World.prototype.NewEvidence = function (parent, template) {
    Evidence.call(this, parent, template);
};

World.prototype.NewEvidence.prototype = Object.create(World.prototype.Evidence.prototype);
World.prototype.NewEvidence.prototype.constructor = World.prototype.NewEvidence;
World.prototype.NewEvidence.prototype.set = function () {
    return World.prototype.Evidence.prototype.set.call(this);
};



world = new World([], [], [], 'client_only', animationLoop, [], 'client_only');

world.createRooms();
world.homesSetup();
world.worldSegmentBuild();
world.setPathNodes();
var d = new Date();
var n = d.getTime();

function animationLoop() {
    if (world.steps === 50) {
        world.charCreate('test', 'npc');
    }
    if (world.steps % 300 === 0) {
        world.chars['test'].calculatePath(world.chars['test'],
                {x: world.rand(40, 1100), y: world.rand(40, 720)});
    }
    this.charFn();
    d = new Date();
    var frame = n - d.getTime();
    d = new Date();
    n = d.getTime();
    setTimeout(function () {
        world.animationLoop();
    }, 24);
}

world.animationLoop();

function distance(a, b) {
    return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
}

function findAngle(a, b, c) { // from two points and a distance, C
    var angle = 180 / 3.14 * Math.acos((a.y - b.y) / c);
    if (a.x > b.x) {
        angle *= -1;
    }
    angle += 270;
    if (angle >= 360) {
        angle -= 360;
    }
    return angle;
}

function  Node(Parent, Point)
{
    var newNode = {
        // pointer to another Node object
        Parent: Parent,
        // array index of this Node in the world linear array
        value: Point.x + (Point.y * 1300),
        neighbours: Point.neighbours,
        index: Point.index,
        path: Point.path,
        // the location coordinates of this Node
        x: Point.x,
        y: Point.y,
        // the distanceFunction cost to get
        // TO this Node from the START
        f: 0,
        // the distanceFunction cost to get
        // from this Node to the GOAL
        g: 0,
        h: 0
    };
    return newNode;
}
